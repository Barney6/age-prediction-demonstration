from demonstration import *


# ---- Set parameters below ----

prototxt = "age.prototxt"
model = "dex_chalearn_iccv2015.caffemodel"

path_to_images = "/Users/shaunbarney/Documents/MachineLearning/MikeAge/"

# ------------------------------

demo = Demonstration(prototxt=prototxt, model=model)

# Capture face demo uses the web cam
capture_image_demo = False
batch_demo = not capture_image_demo

if capture_image_demo:
    # Capture an image and locate the face
    image, box = demo.capture_face(imshow=True)

    # Crop the image
    cropped_image = demo.image_crop(image, box)

    demo.plot_im(demo.face_with_location)

    # Run the image through network
    age = demo.run_net(image=cropped_image)

    print("Predicted age: %.2f" % age)

# Pass path of the images folder
if batch_demo:
    results = demo.run_image_batch(path_to_images=path_to_images, save_image=False)
    for result in results:
        print("Image %s predicted age %.2f" % (result[0], result[1]))

        # for (neuron, activation) in result[2]:
        #     print("{}: {}".format(neuron, activation))
        plot_distribution(result[0], result[2])