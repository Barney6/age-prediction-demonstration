import cv2
import numpy as np
from PIL import Image
import caffe
import os
import matplotlib.pyplot as plt


def plot_distribution(image_name, data):
    neurons, activations = [], []
    for (neuron, activation) in data:
        neurons.append(neuron)
        activations.append(activation)
    neurons = np.array(neurons)
    activations = np.array(activations)
    fig, ax = plt.subplots()
    ax.bar(neurons, activations)
    ax.set_title(image_name)
    plt.show()


class Demonstration:
    def __init__(self, prototxt, model):
        """
        Parameters
        ----------
        :param prototxt : str
            Path to protxt file
        :param model : str
            Path to model file
        :param camera_port : int
            Web camera port
        """
        caffe.set_mode_cpu()
        self.net = caffe.Net(prototxt, model, caffe.TEST)
        self.face_location = []
        self.image = []
        self.face_with_location = []
        self.ret_val = 0
        self.process_this_frame = True
        self.faceCascade = cv2.CascadeClassifier("haarcascade.xml")

    def capture_face(self, imshow, camera_port=0):
        """
        Captures an image once a face has been located.

        Processes every other frame for efficiency.
        
        Parameters
        ----------
        :param imshow : boolean
            Used to show live feed
            
        Returns
        -------
        :return image : numpy.ndarray
            The image captured
        :return : tuple
            The location of the face detected [(left, top, right, bottom)]
       """
        camera = cv2.VideoCapture(camera_port)
        while len(self.face_location) is 0:
            self.ret_val, image = camera.read()
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            self.face_with_location = np.copy(image)
            self.image = np.copy(image)
            small_frame = cv2.resize(self.face_with_location, (0, 0), fx=0.25, fy=0.25)

            if self.process_this_frame:
                self.locate_face(small_frame)

            self.process_this_frame = not self.process_this_frame

            for (x, y, w, h) in self.face_location:
                left = x * 4
                top = y * 4
                right = w * 4
                bottom = h * 4
                cv2.rectangle(self.face_with_location, (left, top), (right, bottom), (0, 255, 0), 2)
                self.face_location = [(left, top, right, bottom)]

            if imshow:
                cv2.imshow('Video', cv2.cvtColor(self.face_with_location, cv2.COLOR_RGB2BGR))

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cv2.destroyWindow('Video')
        cv2.waitKey(1)
        return self.image, self.face_location

    def locate_face(self, image):
        """
        Locate face within an image
        
        Parameters
        ----------
        :param image : numpy.ndarray
            The image to be processed
        :return : tuple
            (left, top, right, bottom)
        """
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        self.face_location = self.faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE
        )
        for (x, y, w, h) in self.face_location:
            self.face_location = [(x, y, x + w, y + h)]

        return self.face_location

    @staticmethod
    def image_crop(image, box, crop_margin=[(0.4, 0.4, 0.4, 0.4)], save_image=True, save_name="cropped_image.jpg"):
        """
        Crops image with default 40% margin.

        Parameters
        ----------
        :param image : numpy.ndarray
            The image to be cropped
        :param box : tuple
            The face location within the image (left, top, right, bottom)
        :param crop_margin : tuple
            The crop margin to be added (default 40%)
        :param save_image : boolean
            Save image as jpg
        :param save_name : str
            Name to save the image as

        Reuturn
        -------
        :return : numpy.ndarray
            The cropped image
        """
        if box:
            dleft = (box[0][2] - box[0][0]) * crop_margin[0][0]
            left = box[0][0] - dleft
            if left < 0:
                left = 0

            dright = (box[0][2] - box[0][0]) * crop_margin[0][2]
            right = box[0][2] + dright
            if right < 0:
                right = 0

            dtop = (box[0][3] - box[0][1]) * crop_margin[0][1]
            top = box[0][1] - dtop
            if top < 0:
                top = 0

            dbottom = (box[0][3] - box[0][1]) * crop_margin[0][3]
            bottom = box[0][3] + dbottom
            if bottom < 0:
                bottom = 0

            new_image = Image.fromarray(image)
            new_image = new_image.crop(box=(int(left), int(top), int(right), int(bottom)))
            if save_image:
                new_image.save(save_name)
            ret_im = np.array(new_image)
        else:
            ret_im = image
        return ret_im

    def run_net(self, path_to_image="", image=None):
        """
        Run the image through the caffe net.

        Parameters
        ----------
        :param path_to_image : str
            Location of the image
            
        Returns
        -------
        :return age : float
            The predicted age
        """

        transformer = caffe.io.Transformer({'data': self.net.blobs['data'].data.shape})
        transformer.set_transpose('data', (2, 0, 1))
        transformer.set_channel_swap('data', (2, 1, 0))
        transformer.set_raw_scale('data', 255.0)
        if path_to_image:
            image = caffe.io.load_image(path_to_image)
        elif image is None:
            raise ValueError("Path_to_image and image can't both be empty.")

        self.net.blobs['data'].data[...] = transformer.preprocess('data', image)

        out = self.net.forward()
        return self.__calc_age(out)

    @staticmethod
    def __calc_age(out):
        """
        Calculates the predicted age.
        
        :param out : dict
            output blob
        :return : float
            predicted age
        """
        age = 0.0
        for i in range(len(out['prob'][0])):
            age += (i * out['prob'][0][i])

        return age

    def run_image_batch(self, path_to_images, save_image=False):
        """
        Run image folder through network
        
        Parameters
        ----------
        :param path_to_images : str
            Path to the images folder
        :param save_image : boolean
            Save the image

        Returns
        -------
        :return : list of tuples
            (image_name, predicted_age, distribution: (neuron, value))
        """
        results = []
        image_list = os.listdir(path_to_images)
        for i, image in enumerate(image_list):
            if 'jpg' not in image:
                continue
            print("Processing image %s" % image)
            loaded_image = np.array(Image.open(path_to_images+image))
            box = self.locate_face(loaded_image)
            cropped_image = self.image_crop(image=loaded_image, box=box, save_image=save_image)
            age = self.run_net(image=cropped_image)
            distribution = self.fc_analyse()
            results.append((image, age, distribution))
            if i == 20:
                break
        return results

    def fc_analyse(self):
        activation_values = []
        for neuron in range(len(self.net.blobs['fc8-101'].data[0])):
            activation_values.append((neuron, self.net.blobs['fc8-101'].data[0][neuron]))
        return activation_values

    @staticmethod
    def plot_im(image):
        plt.imshow(image)
        plt.show()